## Snakemake installation

#### Miniconda
The recommended way of obtaining Snakemake is through Miniconda. [See instruction here](https://conda.io/docs/user-guide/install/index.html)
You can find the installers [here](https://conda.io/miniconda.html). Download the version containing **Python 3** compatible with your operating system.

For Unix systems:
```bash
bash Miniconda3-latest-Linux-x86_64.sh
# and follow instruction from here.

# install snakemake
conda install -c bioconda -c conda-forge snakemake
```

Other ways of obtaining snakemake are specified [here](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html).
In particular this should work for a global installation, if you have a working version of Python >= 3.5:
```bash
pip3 install snakemake
#or
easy_install3 snakemake
```