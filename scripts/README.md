## Pipeline overview

1. preprocess: 
`preprocess.r` reads the CEL files, does normalization, selection of probes for each gene, calculate the p.value for Boolean state and the probability of expression (also called weights) according to `scripts/barcode-weights.r`

1. prepare_hairball:
`build-hairball.r` finds the TFs differentially Booleanized between the initial and final condition, and prepares a transcriptional hairball with the interactions occuring among these TFs according to MetaCore from Clarivate Analytics. 
To visualize this network, use the file `tint-DEbool-cutoff{cutoff}.txt`.

1. contextualize:
`cont-cutoff{cutoff}.m` runs `contextualize_2attr.m`. In this script, the optimization of the initial transcriptional hairball is done so that the resulting network has two attractors as similar as possible to the Booleanized experimental data corresponding to the initial and final cellular phenotypes.

1. extract_solutions:
`extract_best_configurations_2phenotype.R` selects the optimization solutions with the best results.

1. extract_one_solution:
 `extract_one_solution.r` extracts from the best optimization solutions one solution, with the least number of interactions removed compared to the initial hairball. *If the resulting GRN does not contain at least 10 TFs, it returns an error.*
 
1. assign_p_on_weights:
 `assign-pexpr-Metacore.r` assigns expression probabilities (weights) to signalling network entities. Some entities contain multiple genes; as these can be complexes or protein families, we randomly assign the expression probability of the first gene to the whole entity.
 
1. assign_pval_bool:
 `assign-1-pval-Metacore.r` assigns (1-Booleanization p.value), which is used to define Boolean state to signalling network entities. Some entities contain multiple genes; as these can be complexes or protein families, we randomly assign the value of the first gene to the whole entity.
 
1. check_reachable:
 `remove-nonExprPathwayObjects.r` uses the Boolean states of the entities to define if interface TFs are expressed and connected to the source nodes in canonical pathways, which we define as "reachable", in the initial cellular state. We use this criteria to define interface TFs that can be activated or inhibited easily by signalling cues, as all signalling components are available (expressed).
 
1. connect_interface_to_GRN:
 `connect-GRN-to-interface.r` connects the GRN obtained from *extract_one_solution* to the reachable interface TFs, using transcriptional interactions from MetaCore. It also prepares the Boolean state and the adjacency matrix of this expanded GRN to run *in silico* perturbations of the interface TFs (`to-perturb-cutoff{cutoff}.txt`).
The list of interface TFs used is presented in `interfaceTFs-cutoff{cutoff}.txt`. If they are less than 100, perturbations of 1 to 4 TFs at the same time will be performed, otherwise only perturbations of 1-3 TFs.

1. perturb_interface:
 `pert-cutoff{cutoff}.m` runs the simulations of exhaustive combinations of interface TFs states, and calculates how many of the GRN TFs change their state once an attractor state is reached.
 
1. sort_perturbations:
 `sort_perturbations.r` collects and sorts the perturbation results, removing combinations that didn't reach an attractor state after 1000 simulation steps. 
 *If there is no combination that flipped the Boolean state of at least 40% of the GRN TFs, prints a message. The results obtained in this case are generally not reliable, so do not recommend to use them.*
 
1. select_best_perturbations:
 `select-best-perturbations.r` retains all the combinations of interface TFs states that obtained the best 3 number of flipped genes (GRN TFs). It discards combinations of interface TFs that only flip the same genes that the single perturbations of their components could also flip.
 
1. generate_idealP:
 `generate-interfaceTFsP.r` turns the frequency of interface TFs states in the best perturbations in a probability distribution. This defines which interface TFs are more likely, and in which state, to induce the changes in the GRN state required to reach the desired cellular phenotype.
 
1. calculate_P_to_interface:
 `mostP_correlation.r` and  `mostP_length.r` calculate the probability of each signalling molecule to activate or inhibit each interface TFs upon its activation or inhibition. They are two variations of the same algorithm, the first taking into account expression correlation among signalling molecules connected by an interaction, the second uses the length of the path between a signalling molecule and an interface TF to change its overall probability.
 
1. calculate_JSD:
 `save-JSD.r` compares via Jensen-Shannon divergence the probability distribution obtained by `generate-interfaceTFsP.r` and  `mostP_correlation.r`/`mostP_length.r`. The signalling molecules are first ranked independently for the two mostP methods, and then assigned the minimum rank across the two. 
 The signalling molecules whose rank is <= 6% * (maximum rank) are returned as candidates for this cellular transition.
  `SSC_MetacorePathways.r` uses the candidates obtained to select significant pathways. It calculates the cumulative source/sink centrality of the candidates in each pathway [1].
 
 
 [1] Naderi Yeganeh,P. and Mostafavi,M.T. (2019) Causal Disturbance Analysis: A Novel Graph Centrality Based Method for Pathway Enrichment Analysis. IEEE/ACM Trans. Comput. Biol. Bioinforma., PP, 1–1.