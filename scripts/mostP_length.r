# calculate the probability of each intermediate reaching the interface TFs, and their effect
# use surprisal as edge weight, use most probable path to define P(intermediate-interfaceTF)
# modify the probability of each intermediate->TF by the length of the path

# signalling network
library(igraph)
graph_str = readRDS("../data/curated_graph.Rdata")

# arguments
args = commandArgs(TRUE)
cutoff=args[1]

# interface TFs
interfaceTFs = read.table(sprintf("interfaceTFs-cutoff%s.txt",cutoff),sep="\t",quote="",stringsAsFactors=FALSE)[,1]

## 1. calculate edge weights and signs
pexpr = read.table("weights_MetacoreObjects.txt",stringsAsFactors=FALSE,row.names=1,sep="\t",quote="")

# assign weights to each edge as surprisal: log(1/p(expr target node))
weights = sapply(E(graph_str),function(x){
	down = head_of(graph_str,x)
	log(1/pexpr[down,2]) # surprisal
})
#assign signs to the edges
signs = ifelse(E(graph_str)$V5=="Activation",1,-1)

# 2. calculate probabilities
library(foreach)
library(doParallel)
numWorkers = min(c(detectCores() - 1,119))
registerDoParallel(numWorkers)

# for each intermediate-interfaceTF couple find most probable paths (min sum of surprisals)
E_paths = foreach(iTF=interfaceTFs, .combine=list, .inorder=FALSE, .multicombine=TRUE, .maxcombine=length(interfaceTFs), .packages="igraph") %dopar% {
	# find "shortest" path from an interface TF to all signalling molecules
	x = shortest_paths(graph_str,iTF,V(graph_str),mode="in",weights=weights,output="epath")
	edges = x$epath
	edges = edges[lengths(edges)>0]
	# for each of the signalling molecules reached:
	P = lapply(edges, function(y){
		p = sum(weights[as.integer(y)])	# calculate sum of surprisals
		s = prod(signs[as.integer(y)])	# calculate sign
		extr = c(as.character(tail_of(graph_str,y[length(y)])), as.character(head_of(graph_str,y[1]))) # extract extremes of path
		c(extr,p,s)
	})
	z = do.call(rbind,P)
	return(cbind(z,lengths(edges)))
}
E_paths = E_paths[lengths(E_paths)>0]
M = do.call(rbind,E_paths)

# turn into a table
X2 = matrix(0,nrow=length(interfaceTFs),ncol=vcount(graph_str),dimnames=list(interfaceTFs,V(graph_str)$name))
for (i in 1:nrow(M)) {
	a = as.character(V(graph_str)[as.integer(M[i,2])]$name)
	b = as.integer(M[i,1])
	s = as.integer(M[i,4])
	l = as.integer(M[i,5])
	p = 1/exp(abs(as.double(M[i,3]))) # transform surprisal back to probability
	X2[a,b] = s* (p * exp(-1*l))	# modify probability by length of path: p'=p*exp(-length); multiply by the sign of the path s
}

# prepare probability of activation and inhibition of each signalling molecule
activateP = X2
activateP[activateP<0] = 0
inhibitP = X2
inhibitP[inhibitP>0] = 0
inhibitP = abs(inhibitP)
rownames(activateP) = paste0(interfaceTFs,"_1")
rownames(inhibitP) = paste0(interfaceTFs,"_0")
calcPs = rbind(activateP,inhibitP)
calcPs = calcPs[,colSums(calcPs)>0] # remove intermediates that don't reach anything
calcPs = apply(calcPs, 2,function(x) x/sum(x)) # normalize so that for each signalling molecule the probabilities sum to 1

write.table(calcPs,sprintf("intermediateToTFsP-length-cutoff%s.txt",cutoff),sep="\t",quote=FALSE)
