# assign 1-pvalue to network objects, to use for define what is expressed and what is not

# read network
library(igraph)
graph_str = readRDS("../data/curated_graph.Rdata")

# read p-values
expr = read.table("genes_pval.txt",sep="\t",quote="",stringsAsFactors=FALSE)
initial_expr = 1 - expr[,2]
names(initial_expr) = expr[,1]

# assign 1-pvalue to entities
pexpr = matrix(NA,nrow=vcount(graph_str),ncol=2,dimnames=list(V(graph_str)$name,c("symbol","p")))
pexpr[,"symbol"] = V(graph_str)$symbol
for (e in rownames(pexpr)) {
                ps=strsplit(pexpr[e,"symbol"],";",fixed=TRUE)[[1]]
				vals = ifelse (length(ps)==0, NA, initial_expr[ps][1])# assign the pvalue assigned to the first gene in the node to the whole node
                vals[which(is.na(vals))]=1
                pexpr[e,"p"]=min(vals)
}
write.table(pexpr,"bool_MetacoreObjects.txt",row.names=TRUE,col.names = FALSE,sep="\t",quote=FALSE)
