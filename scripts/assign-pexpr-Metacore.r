# assign expression probability to network objects

# read network
library(igraph)
graph_str = readRDS("../data/curated_graph.Rdata")

# read p(expression)
expr = read.table("genes_weights.txt",sep="\t",quote="",stringsAsFactors=FALSE) 
initial_expr = expr[,2]
names(initial_expr) = expr[,1]

# assign p(expression) to entities
pexpr = matrix(NA,nrow=vcount(graph_str),ncol=2,dimnames=list(V(graph_str)$name,c("symbol","p")))
pexpr[,"symbol"] = V(graph_str)$symbol
for (e in rownames(pexpr)) {
                ps=strsplit(pexpr[e,"symbol"],";",fixed=TRUE)[[1]]
				vals = ifelse (length(ps)==0, NA, initial_expr[ps][1])# assign the pvalue assigned to the first gene in the object to the whole object
                vals[which(is.na(vals))]=1
                pexpr[e,"p"]=min(vals)
}
write.table(pexpr,"weights_MetacoreObjects.txt",row.names=TRUE,col.names = FALSE,sep="\t",quote=FALSE)
