# calculate Jensen-Shannon divergence between two vectors (prob. distributions)
# output = [0 1]
JSDivergence = function(p,q) {
	m = 0.5 * (p + q)
	LRp = ifelse(p > 0, log2(p/m), 0)
	LRq = ifelse(q > 0, log2(q/m), 0)
	JS = 0.5 * (sum(p * LRp) + sum(q * LRq))
	return(JS)
}