- **TransReg_TFHairball.txt**:	transcriptional regulation interactions among all gene symbols corresponding to transcription factors and co-factors (transcriptional regulators, *TR*) present in AnimalTFDB 2.0 (from Metacore from Clarivate Analytics)

- **TransReg_objectTOgeneSymbol.txt**: TR network objects and corresponding gene symbols

- **tableTFpath-Metacore.txt**:	TR network objects (rows) and presence in the canonical signalling pathways (0=absent,1=present)

- **table-pathwayObjects-Metacore.txt**: same as tableTFpath-Metacore.txt but for all signalling objects

- **MetacoreIntTFobject-GRN_TFs-interactions.txt**: transcriptional regulation interactions among all TRs that belong to canonical pathways (potential interface TFs) and TR gene symbols present in TransReg_TFHairball.txt (from Metacore from Clarivate Analytics)

- **graph_str-*****[pathway]*****.Rdata**: igraph objects each containing one canonical signalling pathway from Metacore from Clarivate Analytics

- **pathway_names.txt**: complete names of each pathway

- **curated_graph.Rdata**: igraph object containing the complete signalling network used for the analysis (from Metacore from Clarivate Analytics)
    Vertex attributes: name=network object, symbol=gene symbol(s); edge attributes: V5=effect (activation or inhibition), V6=mechanism

- **correlation_edges_0p7-CURATED.txt**: gene expression correlation among the extremes of each interaction present in curated_graph.Rdata, calculated across all CMap datasets. Only absolute values >0.7 are reported

- **Metacore_signedpathway_adjmatrix.Rdata**: list of adjacency matrices of the signed Metacore signalling pathways