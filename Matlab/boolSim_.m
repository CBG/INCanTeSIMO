function [x1, i] = boolSim_(p, x, l)
%function [x1, i] = boolSim_(p, x, l)
%x1: attractor state
%i: iterations done						   
%p: adjecency matrix
%x: initial expression state
%l: number of max iterations to test

%    Copyright (C) 2018 Andras Hartmann <andras.hartmann@gmail.com>
%
%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this program.  If not, see <http://www.gnu.org/licenses/>.


% To have the same interface as previously
p = p';

%% initializing
n = length(x);
x1 = -inf(size(x));

% Nodes with incoming activation edges
act = (p>0)*ones(n,1)>0;

% Nodes with incoming inhibition edges
inh = (p<0)*ones(n,1)>0;

for i=1:l+1

    % both activation and inhibition
    pos = act&inh;
    if ~isempty(x(pos))
        x1(pos) = p(pos,:)*x >=1;
    end


    % only activation and no inhibition
    pos = act&~inh;
    if ~isempty(x(pos))
        x1(pos) = p(pos,:)*x >=1;
    end

    % no activation only inhibition
    pos = ~act&inh;
    if ~isempty(x(pos))
        %x1(pos) = p(pos,:)*x >=0;
        x1(pos) = p(pos,:)*x >0;
    end
    
    % no activation no inhibition
    % nodes with no incoming edges at all
    pos = ~act&~inh;
    if ~isempty(x(pos))
        x1(pos) = x(pos);
    end

    %noedges = (sum(abs(p),2)==0);
    %x1(noedges) = x(noedges);
    
    %only inhibitors
    %no_enh = ((double(p>0)*ones(n,1))==0);
    
    %x1 = x1|myzeros&no_enh;
%	x1 = x1|(~(double(p>0)*x(1:n)))&(~(double(p<0)*x(1:n)));
    if x1 == x
        break;
    end
    x = x1;
    %if p*x(1:n)==0
%	   ;

end



