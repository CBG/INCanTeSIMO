#!/bin/bash -l
#SBATCH -J 608__98
#SBATCH -t 5-0
#SBATCH -N 1 --ntasks-per-node=1 -c 12

# modules
module load swenv/default-env/v0.1-20170602-production
module load lang/R/3.4.0-foss-2017a-X11-20170314-bare
module load base/MATLAB/2017a

# ask snakemake to create the results file candidates-cutoff0.95-bestcomb3.txt
snakemake -s ../Snakefile candidates-cutoff0.95-bestcomb3.txt &> cutoff0.95-bestcomb3.log
