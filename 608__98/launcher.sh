#!/bin/bash -l
#OAR -n 608__98
#OAR -l nodes=1/core=12,walltime=120

# add snakemake to the $PATH if needed, e.g. 
# export PATH=$PATH:/mnt/gaiagpfs/users/homedirs/gzaffaroni/miniconda3/envs/snakemake-tutorial/bin
# (you can find where snakemake is with: $> whereis snakemake)

# load the correct versions of MATLAB and R
module load base/MATLAB/2017a
module load lang/R/3.4.0-foss-2017a-X11-20170314-bare

# ask snakemake to create the results file candidates-cutoff0.95-bestcomb3.txt
snakemake -s ../Snakefile candidates-cutoff0.95-bestcomb3.txt &> cutoff0.95-bestcomb3.log

