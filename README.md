INCAnTeSIMO (italian for enchantment): Integrated Network approach for Cellular Transitions through SIgnalling MOlecules

This repository presents a computational method that predicts signalling molecules and pathways inducing cellular transitions using gene expression profiles.

See the associated paper: 

Gaia Zaffaroni, Satoshi Okawa, Manuel Morales-Ruiz, Antonio del Sol, An integrative method to predict signalling perturbations for cellular transitions, Nucleic Acids Research, , gkz232, [https://doi.org/10.1093/nar/gkz232](https://doi.org/10.1093/nar/gkz232)

To install it:
```bash
git clone https://git-r3lab.uni.lu/gaia.zaffaroni/INCanTeSIMO.git path/to/workdir
cd path/to/workdir
```

## Snakemake
The Snakemake workflow management system is a tool to create reproducible and scalable data analyses. Workflows are described via a human readable, Python based language. You can [find documentation here](https://snakemake.readthedocs.io/en/stable/index.html).
For short installing instructions, see [here](./snakemake_INSTALL.md).

Running a pipeline with Snakemake, you only need to specify which file you want to generate, and the necessary steps (called *rules*) will be recognized and run automatically.
Here, the file [Snakefile](./Snakefile) defines the rules and their dependencies.

## Requirements
The pipeline contains R and Matlab scripts. It has been tested on Unix environment with MATLAB 2017a and R-3.4.0.
#### R packages
To test if you have the required packages, and automatically install the missing ones and their dependencies, run `Rscript R_setup.r`. You can see which version of each package you have in the output file `R_sessionInfo.txt`. Make sure that you have **igraph 1.1.1 or later**.

#### Matlab toolboxes and functions:
* Matlab BGL (provided with this repo), see info at: https://launchpad.net/matlab-bgl 

## Expression data
The pipeline takes advantage of *frma* and the *Gene Expression Barcode* (1, 2, 3) to booleanize the expression values and define which genes are expressed or not, and assign them an expression probability. Compatible microarray platforms are:

|Platform GEO ID | Platform name | frozen vectors|
|--- | --- | ---|
|GPL96 | Affymetrix Human Genome U133A | hgu133afrmavecs|
|GPL570 | Affymetrix Human Genome U133 Plus 2.0 | hgu133plus2frmavecs|
|GPL571 | Affymetrix Human Genome U133A 2.0 | hgu133a2frmavecs|
|GPL6244 | Affymetrix Human Gene 1.0 ST Array | hugene.1.0.st.v1frmavecs|
|GPL6246 | Affymetrix Mouse Gene 1.0 ST Array | mogene.1.0.st.v1frmavecs|
|GPL1261 | Affymetrix Mouse Genome 430 2.0 | mouse4302frmavecs|

To define which samples correspond to the initial and query cellular state, prepare a file `samples.txt` (tab-separated) containing:
```
dataset class
EC2004111108AA.CEL	control
EC2004111112AA.CEL	treated
```
with control=the initial state, and treated=the desider state. Only `control` and `treated` are accepted in `class`, however multiple samples can be specified for each of them.

## Parameters
There are 2 parameters in the pipeline: 
* **cutoff** = is used to define expressed genes. It represents the probability of not belonging to the non-expressed distribution (~ of being expressed). If a probe has value &lt; cutoff, it is not expressed, if it is &gt;= cutoff it is expressed. The default cutoff is 0.95.
* **bestcomb** = the number of best interface TF combinations (BPCs) kept, with ties. The default is 3 (meaning all the combinations that get the 3 highest number of GRN-TFs to change their state, are used to calculate the interface TFs likelihood of acting on the GRN).

The parameters are directly specified in the name of the file you want to generate: candidates-cutoff0.95-bestcomb3.txt

## Run an example
The data for the CMap dataset 608__98 is contained in this repository. To run the pipeline:
```bash
cd 608__98/
snakemake -s ../Snakefile candidates-cutoff0.95-bestcomb3.txt &> cutoff0.95-bestcomb3.log
```
If you are running this pipeline on a server (e.g. HPC), you might want to use a launcher to load all the necessary software, see for example [launcher.sh](./608__98/launcher.sh)(OAR scheduler) or [iris_launcher.sh](./608__98/iris_launcher.sh)(SLURM scheduler).

Depending on the resources available, **the pipeline might take a very long time to run**. The results presented in the paper were obtained with 12 cores and a running time of up to 5 days. This particular example should be done in around 60 minutes.

The most time consuming step is the perturbation of combinations of interface TFs (in particular, obtaining the combinations of 4 TFs at the same time). Therefore, if there are more than 100 interface TFs, only combinations of up to 3 TFs are perturbed.

At the end of the pipeline, `candidates-cutoff0.95-bestcomb3.txt` will contain the list of candidates for the transition from the initial to the final cellular state (Note that this is the file that we asked Snakemake to create). At the same time, the pathways predicted according to the source/sink centrality concept (introduced in (4)) are saved in `pathways-cutoff0.95-bestcomb3.txt`.

#### Errors
There are 3 main sources of errors:
1. The GRN is too small (<10 TFs): the TFs that are differentially booleanized between the two cellular states are too few or disconnected. It might be worth checking the samples used for each state (are they good replicates, or are they fairly different? Are there other datasets for the same cellular state available?). 

    This will be stated in the snakemake log file (i.e. `cutoff0.95-bestcomb3.log`) as: `"The contextualized GRN contains # TFs. NOT BIG ENOUGH"`.
1. The perturbations of interface TFs do not manage to change the state of at least 40% of the TFs in the GRN. There is no solution for this at the moment, the present method cannot generate predictions for this cellular transition.

    This will be stated in the snakemake log file as: `"Max flipping is # %. NOT FLIPPING ENOUGH"`.
1. The pipeline does not complete in the time allocated for it. In this case, the directory might be protected by snakemake, so you need to unlock it with `snakemake -s ../Snakefile candidates-cutoff0.95-bestcomb3.txt --unlock` before re-launching it. Only the missing steps will be run.

## Pipeline
Find a description of what the different rules and scripts do [here](./scripts/README.md).

![dag](./dag.png)

Each node represents a rule, and an arc exists between two rules if the output of one is an input for the other. It is specified when cutoff and bestcomb parameters are used.

## Prior knowledge newtorks
The signalling pathways and the transcriptional interactions used in this analysis pipeline are obtained from [MetaCore from Clarivate Analytics](https://clarivate.com/products/metacore/).
MetaCore is empowered by MetaBase, "arguably the most comprehensive manually curated database of mammalian biology data available today in both industry and academia."

## References
1) McCall, Matthew N., Benjamin M. Bolstad, and Rafael A. Irizarry. 2010. “Frozen Robust Multiarray Analysis (FRMA).” Biostatistics 11 (2): 242–53. doi:[10.1093/biostatistics/kxp059](https://doi.org/10.1093/biostatistics/kxp059).

2) McCall, Matthew N, Karan Uppal, Harris A Jaffee, Michael J Zilliox, and Rafael A Irizarry. 2011. “The Gene Expression Barcode: Leveraging Public Data Repositories to Begin Cataloging the Human and Murine Transcriptomes.” Nucleic Acids Research 39 (Database issue): D1011-5. doi:[10.1093/nar/gkq1259](https://doi.org/10.1093/nar/gkq1259).

3) McCall, Matthew N, Harris A Jaffee, Susan J Zelisko, Neeraj Sinha, Guido Hooiveld, Rafael A Irizarry, and Michael J Zilliox. 2014. “The Gene Expression Barcode 3.0: Improved Data Processing and Mining Tools.” Nucleic Acids Research 42 (D1). Oxford University Press: D938–43. doi:[10.1093/nar/gkt1204](https://doi.org/10.1093/nar/gkt1204).

4) Naderi Yeganeh, Pourya, and M. Taghi Mostafavi. 2019. “Causal Disturbance Analysis: A Novel Graph Centrality Based Method for Pathway Enrichment Analysis.” IEEE/ACM Transactions on Computational Biology and Bioinformatics PP (March): 1–1. doi:[10.1109/TCBB.2019.2907246](https://doi.org/10.1109/TCBB.2019.2907246).