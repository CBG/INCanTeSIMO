rule all:
	input: "candidates-cutoff{cutoff}-bestcomb{bestcomb}.txt"

rule preprocess:
	input:
		"samples.txt"
	output:
		pval = "genes_pval.txt",
		weights = "genes_weights.txt"
	shell:
		"Rscript ../scripts/preprocess.r"
	
	
rule prepare_hairball:
	input:
		"genes_pval.txt"
	output:
		adj="adjmat-DEbool-cutoff{cutoff}.txt", 
		ph="phenored-DEbool-cutoff{cutoff}.txt", 
		cont="cont-cutoff{cutoff}.m", 
	shell:
		"Rscript ../scripts/build-hairball.r {wildcards.cutoff}"

rule contextualize:
	input:
		script=rules.prepare_hairball.output.cont,
		b=rules.prepare_hairball.output.adj,
		c=rules.prepare_hairball.output.ph
	output:
		"DEbool-cutoff{cutoff}-2ph_GA_solutions.txt"
	shell:
		"matlab -nodisplay -nosplash <'{input.script}'"

rule extract_solutions:
	input:
		rules.contextualize.output
	output:
		"all_contextualized_best_adjacency-cutoff{cutoff}.txt"
	params:
		error=1
	shell:
		"Rscript ../scripts/extract_best_configurations_2phenotype.R {input} {params.error} {wildcards.cutoff}"
		
rule extract_one_solution:
	input:
		a=rules.prepare_hairball.output.adj,
		b=rules.extract_solutions.output
	output:
		"interactions_least_pruned-cutoff{cutoff}.txt"
	shell:
		"Rscript ../scripts/extract_one_solution.r {input.a} {input.b} {wildcards.cutoff}"
		
	
rule assign_p_on_weights:
	input:
		rules.preprocess.output.weights
	output:
		"weights_MetacoreObjects.txt"
	shell:
		"Rscript ../scripts/assign-pexpr-Metacore.r"

rule assign_pval_bool:
	input:
		rules.preprocess.output.pval
	output:
		"bool_MetacoreObjects.txt"
	shell:
		"Rscript ../scripts/assign-1-pval-Metacore.r" 
			
rule check_reachable:
	input:
		rules.assign_pval_bool.output
	output:
		"table-reachableTFs-cutoff{cutoff}.txt"
	shell:
		"Rscript ../scripts/remove-nonExprPathwayObjects.r {wildcards.cutoff}"
		
rule connect_interface_to_GRN:
	input:
		rules.assign_pval_bool.output,
		rules.check_reachable.output,
		rules.prepare_hairball.output.ph,
		rules.extract_one_solution.output
	output:
		ph="pheno-contextualized-cutoff{cutoff}-interface.txt",
		adj="adjmat-contextualized-cutoff{cutoff}-interface.txt",
		top="to-perturb-cutoff{cutoff}.txt",
		sin="singleTF_to_node-cutoff{cutoff}.txt",
		int="interfaceTFs-cutoff{cutoff}.txt",
		pert="pert-cutoff{cutoff}.m"
	shell:		
		"Rscript ../scripts/connect-GRN-to-interface.r {wildcards.cutoff}"

rule perturb_interface:
	input:
		script=rules.connect_interface_to_GRN.output.pert,
		others=rules.connect_interface_to_GRN.output
	output:
		"cutoff{cutoff}-perturbation_1_interfaceNode.txt",
		"cutoff{cutoff}-perturbation_2_interfaceNode.txt",
		"cutoff{cutoff}-perturbation_3_interfaceNode.txt"
	shell:
		"matlab -nodisplay -nosplash < '{input.script}'"
		
rule sort_perturbations:
	input:
		rules.perturb_interface.output
	output:
		"cutoff{cutoff}_all_ranked_perturbations.txt"
	shell:
		"Rscript ../scripts/sort_perturbations.r {wildcards.cutoff}"
		
rule select_best_perturbations:
	input:
		rules.sort_perturbations.output
	output:
		"bestcomb{bestcomb}_perturbations-cutoff{cutoff}.txt"
	shell:
		"Rscript ../scripts/select-best-perturbations.r {wildcards.cutoff} {wildcards.bestcomb}"
	
rule generate_idealP:
	input:
		rules.connect_interface_to_GRN.output,
		rules.select_best_perturbations.output
	output:
		"interfaceTFsP-cutoff{cutoff}-bestcomb{bestcomb}.txt"
	shell:
		"Rscript ../scripts/generate-interfaceTFsP.r {wildcards.cutoff} {wildcards.bestcomb}"
		
rule calculate_P_to_interface:
	input:
		rules.assign_p_on_weights.output,
		rules.connect_interface_to_GRN.output.int
	output:
		"intermediateToTFsP-correlation-cutoff{cutoff}.txt",
		"intermediateToTFsP-length-cutoff{cutoff}.txt"
	shell:
		"Rscript ../scripts/mostP_correlation.r {wildcards.cutoff};"
		"Rscript ../scripts/mostP_length.r {wildcards.cutoff}"

rule calculate_JSD:
	input:
		rules.generate_idealP.output,
		rules.calculate_P_to_interface.output
	output:
		"JSDs-cutoff{cutoff}-bestcomb{bestcomb}.txt",
		"minRanks-cutoff{cutoff}-bestcomb{bestcomb}.txt",
		"candidates-cutoff{cutoff}-bestcomb{bestcomb}.txt",
		"pathways-cutoff{cutoff}-bestcomb{bestcomb}.txt"
	shell:
		"Rscript ../scripts/save-JSDs.r {wildcards.cutoff} {wildcards.bestcomb};"
		"Rscript ../scripts/SSC_MetacorePathways.r {wildcards.cutoff} {wildcards.bestcomb}"