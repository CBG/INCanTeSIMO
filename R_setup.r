# ipak function: install and load multiple R packages.
# check to see if packages are installed. Install them if they are not, then load them into the R session.
# from https://gist.github.com/stevenworthington/3178163
ipak <- function(pkg){ 
    new.pkg <- pkg[!(pkg %in% installed.packages()[, "Package"])]
    if (length(new.pkg)) install.packages(new.pkg, dependencies = TRUE, repos='http://cloud.r-project.org/')
	sapply(pkg, require, character.only = TRUE)
}

source("https://bioconductor.org/biocLite.R")
ipak_bioC <- function(pkg){ 
    new.pkg <- pkg[!(pkg %in% installed.packages()[, "Package"])]
    if (length(new.pkg)) biocLite(pkg)
	sapply(pkg, require, character.only = TRUE)
}


required_packages_bioC = c(
	# for microarray processing
	"affy",
	"oligo",
	"frma",
	# "GEOquery", # de-comment if you want to automatically download CEL files from GEO

	# gene expression distributions for Gene Expression Barcode (McCall 2011)
	"hgu133afrmavecs",
	"hgu133plus2frmavecs",
	"hgu133a2frmavecs",
	"mouse4302frmavecs",
	"hugene.1.0.st.v1frmavecs",
	"mogene.1.0.st.v1frmavecs",

	# annotation packages
	"biomaRt",
	"hgu133plus2.db",
	"hgu133a.db",
	"hgu133a2.db",
	"mouse4302.db",
	"hugene10sttranscriptcluster.db",
	"mogene10sttranscriptcluster.db",
	
	# for CADIA
	"KEGGgraph",
	"graph"
)

required_packages_CRAN = c(
	# for the analysis
	"igraph",
	"foreach",
	"doParallel",
	"stringr",
	
	# for CADIA
	"devtools",
	"RBGL",
	"dplyr",
	"magrittr"
)

ipak(required_packages_CRAN)
ipak_bioC(required_packages_bioC)

# CADIA from github
library(devtools)
devtools::install_github("pouryany/CADIA")

sink("R_sessionInfo.txt")
sessionInfo()
sink()